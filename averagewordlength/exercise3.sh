#!/bin/sh
ant -f build.xml
hadoop fs -rm -r wordlengths
hadoop jar averagewordlength.jar solution.AvgWordLength shakespeare wordlengths
hadoop fs -cat wordlengths/*
