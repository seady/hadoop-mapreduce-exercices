package stubs;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Example input line:
 * 96.7.4.14 - - [24/Apr/2011:04:20:11 -0400] "GET /cat.jpg HTTP/1.1" 200 12433
 *
 */
public class ImageCounterMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
	Text textObj = new Text();
	IntWritable intObj = new IntWritable();
	Pattern imgPtn = Pattern.compile("GET /.*(\\.[a-zA-Z]+)\\s");
	Matcher mth = null;
	
  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException 
  {
	  mth = imgPtn.matcher(value.toString());
	  if(mth.find())
	  {
		  String imgName = mth.group(1).toLowerCase();
		  context.getCounter(ImageCounter.ImageGrpName, imgName).increment(1);
	  }
	  else
	  {
		  context.getCounter(ImageCounter.ImageGrpName, "Others").increment(1);
	  }
  }
}