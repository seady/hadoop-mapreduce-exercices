#!/bin/sh
hadoop fs -rm -r output
hadoop jar logging.jar solution.AvgWordLength -Dmapred.map.child.log.level=WARN shakespeare output
echo "Check http://localhost:50030/jobtracker.jsp"
