#!/bin/sh
ant -f build.xml
hadoop fs -rm -r toolrunnerout
hadoop jar toolrunner.jar solution.AvgWordLength -DcaseSensitive=false shakespeare toolrunnerout
#hadoop jar toolrunner_hints.jar solution.AvgWordLength -DcaseSensitive=false shakespeare toolrunnerout
hadoop fs -cat toolrunnerout/*
