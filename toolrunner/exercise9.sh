#!/bin/sh
ant -f build.xml
rm -rf localout
hadoop jar toolrunner.jar solution.AvgWordLength -fs=file:/// -jt=local \
~/training_materials/developer/data/shakespeare localout
cat localout/*
