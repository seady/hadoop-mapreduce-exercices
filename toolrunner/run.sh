#!/bin/sh
. ~/.bashrc
hfs -rm -r toolrunnerout
hadoop jar toolrunner_hints.jar hints.AvgWordLength -DcaseSensitive=true shakespeare toolrunnerout
#hadoop jar toolrunner_hints.jar solution.AvgWordLength -DcaseSensitive=false shakespeare toolrunnerout
hfs -cat toolrunnerout/*
