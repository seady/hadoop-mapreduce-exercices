#!/bin/sh
. ~/.bashrc
rm -rf localout
hadoop jar toolrunner_hints.jar hints.AvgWordLength \
-fs=file:/// -jt=local -DcaseSensitive=true \
~/training_materials/developer/data/shakespeare localout
#hadoop jar toolrunner_hints.jar solution.AvgWordLength -DcaseSensitive=false shakespeare toolrunnerout
cat localout/*
